nnoremap <silent> <leader>sn :TestNearest<CR>
nnoremap <silent> <leader>sf :TestFile<CR>
nnoremap <silent> <leader>ss :TestSuite<CR>
nnoremap <silent> <leader>sl :TestLast<CR>

let g:test#strategy = 'dispatch'
