" This is the main plugin list, -sourced by modules/plugins.vim
" Configuration goes in the appropriate modules/plugins/*.vim file.
" So configuration for tmux.vim would go in modules/plugins/tmux.vim.vim

Plug 'Keithbsmiley/tmux.vim'
Plug 'Lokaltog/vim-distinguished'
Plug 'altercation/vim-colors-solarized'
Plug 'Lokaltog/vim-easymotion', { 'on': '<Plug>(easymotion-prefix)' }
Plug 'PeterRincker/vim-argumentative'
Plug 'Raimondi/delimitMate'
Plug 'Shougo/neocomplete.vim'
Plug 'Wolfy87/vim-enmasse', { 'on': 'EnMasse' }
Plug 'Wolfy87/vim-expand', { 'on': 'Expand' }
Plug 'amdt/vim-niji', { 'for': ['clojure', 'javascript'] }
Plug 'bling/vim-airline'
Plug 'embear/vim-localvimrc'
Plug 'guns/vim-clojure-highlight', { 'for': 'clojur' }
Plug 'guns/vim-clojure-static', { 'for': 'clojure' }
Plug 'helino/vim-json', { 'for': 'json' }
Plug 'janko-m/vim-test'
Plug 'junegunn/fzf'
Plug 'marijnh/tern_for_vim', { 'do': 'npm install', 'for': 'javascript' }
Plug 'mhinz/vim-signify'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'pangloss/vim-javascript', { 'for': 'javascript' }
Plug 'rking/ag.vim', { 'on': 'Ag' }
Plug 'scrooloose/syntastic'
Plug 'sjl/gundo.vim', { 'on': 'GundoToggle' }
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-markdown'
Plug 'tpope/vim-projectionist'
Plug 'tpope/vim-ragtag', { 'for': ['html', 'xml'] }
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-vinegar'
Plug 'vim-scripts/paredit.vim', { 'for': 'clojure' }
Plug 'walm/jshint.vim', { 'for': 'javascript' }
Plug 'xolox/vim-easytags'
Plug 'xolox/vim-misc'
Plug 'xolox/vim-session'
